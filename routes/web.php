<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'TarjotinController@list_all');

Route::post('/searchcourses', 'TarjotinController@search');

Route::group(['middleware' => ['auth', 'admin']], function() {

    Route::get('/courseadd', function () {
        return view('courseadd');
    });
    Route::get('/groupadd', function () {
        return view('groupadd');
    });
    
    Route::post('/searchadmin', 'TarjotinController@search_admin');

    Route::get('/home', 'TarjotinController@admin_list_all');
    Route::get('/admintools', 'TarjotinController@admin_list_all');

    Route::get('/courseform', 'TarjotinController@admin_modify');
    Route::patch('/courseform/{id}', 'TarjotinController@admin_update');
    Route::delete('/courseform/{id}', 'TarjotinController@admin_delete');
    Route::get('/courseadd', 'TarjotinController@admin_add_check_course');
    Route::post('/courseadd', 'TarjotinController@admin_add');

    Route::get('/groupform', 'TarjotinController@admin_group_modify');
    Route::patch('/groupform/{gid}', 'TarjotinController@admin_group_update');
    Route::delete('/groupform/{gid}', 'TarjotinController@admin_group_delete');
    Route::get('/groupadd', 'TarjotinController@admin_add_check_group');
    Route::post('/groupadd', 'TarjotinController@admin_group_add');
});