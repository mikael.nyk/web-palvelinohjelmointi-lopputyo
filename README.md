[dbOverview]: src/dbOverview2.png
[overviewOfApp]: src/overviewOfApp.png
[overviewOfStruct]: src/overviewOfStruct.png

Kurssi: Web-palvelinohjelmointi KESÄ 2021 - Lopputyö

Tekijät: Mikael Nykänen (AB4510), Aleksi Perälä (P1725) - SeAMK - 12/08/2021

-----------------------------------------------------------------------------------------------------------------------------------------------

# Kurssitarjotinsovellus

Valitsimme aiheeksemme valmiiksi tarjotun aiheen "Kurssitarjotinsovellus". Aloitimme työn pohtimalla käyttöliittymän eri osa-alueita.
Työssä tulee olla kaikille näkyvä kurssit listaava osa ja pääkäyttäjälle oma osio kurssien muokkaamiselle, poistamiselle ja lisäämiselle.

Järjestelmänvalvojan kirjautumistunnus:`admin@student.jamk.fi` // salasana: `sala1234`

Linkki tehtävään: https://student.labranet.jamk.fi/~AB4510/projekti01/public/ 

Linkki videoesittelyyn: https://youtu.be/8_sSJG5UPuQ 

## __1. Tietokannan luonti__

Aloitimme työn tekemisen suunnittelemalla siihen soveltuvan tietokannan. Hetken aikaa mietittyämme, päädyimme kolmen taulun malliin. Loimme aluksi `kurssiryhma` taulun, jonne tuli `ryhma_id` ja `ryhma_nimi` sarakkeet. Seuraavaksi teimme `kurssit` taulun, jonne sarakkeiksi `kurssi_id`, `ryhma_id` (viittaa kurssiryhma.ryhma_id), `haku_id` (viittaa hakusanat.haku_id), `kurssi_nimi` ja `kurssi_ects`. Kolmantena tauluna loimme `hakusanat` taulun, jonne loimme sarakkeiksi `haku_id`, `ryhma_id` ja `hakusana`.

![Yleiskatsaus tietokannasta][dbOverview]

Pöytien SQL lauseet:

``` sql
CREATE TABLE `kurssiryhma` (
  `ryhma_id` INT PRIMARY KEY AUTO_INCREMENT,
  `ryhma_nimi` VARCHAR(30)
);

CREATE TABLE `kurssit` (
  `kurssi_id` INT PRIMARY KEY AUTO_INCREMENT,
  `ryhma_id` INT,
  `haku_id` INT,
  `kurssi_nimi` VARCHAR(30),
  `kurssi_ects` INT
);

CREATE TABLE `hakusanat` (
  `haku_id` INT PRIMARY KEY AUTO_INCREMENT,
  `ryhma_id` INT,
  `hakusana` VARCHAR(100)
);
```

Pöytien luonnin jälkeen meidän täytyi vielä muokata niitä, jotta järjestelmänvalvojan tekemät tiedon lisäämiset, muokkaamiset ja poistamiset, eivät vaikuttaisi tietokannan rakenteelliseen eheyteen negatiivisesti. Lopuksi täytimme tietokannan itsekeksimillä kursseilla, hakusanoilla ja ryhmillä.

``` sql
ALTER TABLE kurssit ADD FOREIGN KEY (`haku_id`) REFERENCES hakusanat (`haku_id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE kurssit ADD FOREIGN KEY (`ryhma_id`) REFERENCES kurssiryhma (`ryhma_id`) ON UPDATE CASCADE ON DELETE SET NULL;

INSERT INTO kurssiryhma (ryhma_nimi) VALUES ("Ohjelmointi"), ("Linux"), ("Android"), ("Office"), ("Tietoturva"), ("Sulautetut");

INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (1,"Rust perusteet",4), (1,"C# perusteet",3), (1,"Java perusteet",2), (1,"Rust jatkokurssi",4), (1,"PHP perusteet", 3);
INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (2,"Linux perusteet",2), (2,"Linux edistynyt käyttäminen",6), (2,"Terminaalin edistynyt käyttö",3), (2,"Ubuntu Palvelinhallinta",4);
INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (3,"Kotlinin alkeet",3), (3,"Androidin perusteet",2), (3,"Appien perusteet",2);
INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (4,"Excel perusteet",2), (4,"Excel jatkokurssi",3), (4,"Powerpoint perusteet",2);
INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (5,"Tietoturva yleisesti",2), (5,"Palomuurit",4), (5,"Yritysverkon suojaaminen",4);
INSERT INTO kurssit (ryhma_id, kurssi_nimi, kurssi_ects) VALUES (6,"Elektroniikka",3), (6,"Mikro-ohjaimet",4), (6,"Ohjelmoitava logiikka",4);

INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (1, "ohjelmointi java c# rust php ohjelmointi c++");
INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (2, "linux käyttöjärjestelmä os unix torvalds");
INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (3, "ohjelmointi android puhelimet kännykkä kännykät");
INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (4, "ms microsoft office excel powerpoint word");
INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (5, "tietoturva palomuuri virus virukset hakkeri suojaus");
INSERT INTO hakusanat (ryhma_id, hakusana) VALUES (6, "sulautetut raspberry mini micro mega at logiikat elektroniikka");
```

## __2. Yleiskatsaus Kurssitarjotin -sovelluksen toiminnasta__

Sovelluksemme kotinäkymänä toimii kaikille käyttäjille avoin kurssien selaamisnäkymä. Kirjautumaton käyttäjä voi selata ja hakea kursseja hakusanoilla. Järjestelmänvalvoja pystyy sen lisäksi editoimaan kursseja ja lisäämään/poistamaan kursseja/kurssiryhmiä. Järjestelmänvalvojalla on myös oma hakukenttänsä kirjautuneena. Se mahdollistaa kurssin muokkaamisen suoraan hakukoneesta tulleesta tuloksesta.

![Yleiskatsaus sovelluksesta][overviewOfApp]

![Yleiskatsaus sovelluksen rakenteesta][overviewOfStruct]

## __3. Funktiot__

Tässä muutamia käyttämiämme funktiota.

``` php
public function admin_list_all(){
    $courses = DB::table('kurssit')->paginate(6);

    return view('admintools')->with('courses', $courses);
}
```
`admin_list_all` funktiolla näytetään järjestelmänvalvojalle kaikki kurssit samalla lailla kuin normaalikäyttäjälle paitsi näkymään lisätään kurssien muokkaamistyökalut.

``` php
public function admin_add(){
    $course_name = request('Name_add');
    $course_ects = request('ECTS_add');
    $group_id = request('GID_add');

    $course = DB::table('kurssit')
    ->insert(['kurssit.ryhma_id' => $group_id,
    'kurssit.kurssi_nimi' => $course_name,
    'kurssit.kurssi_ects' => $course_ects]);

    return redirect('/admintools');
}

public function admin_add_check_course(){
    $min = DB::table('kurssiryhma')
    ->min('kurssiryhma.ryhma_id');

    $max = DB::table('kurssiryhma')
    ->max('kurssiryhma.ryhma_id');

    return view('/courseadd')
    ->with('min', $min)
    ->with('max', $max);
}
```
`admin_add` toimii järjestelmänvalvojalle kurssien lisäämisfunktiona. Funktiolle syötetään parametreina kurssin nimi, opintopisteiden määrä ja ryhmätunnus. 

`admin_add_check_course` funktio tarkistaa, että kyseistä ryhmätunnusta ei ole jo tietokannassa. Funktio lähettää tiedon sitä kutsuvalle näkymälle. Näkymän puolella parametrit syötetään HTML regex tarkistuslauseeseen. Tällä saadaan luotua alkeellinen tarkistus selaimen puolella.

``` php
public function admin_group_update(){
    $gid = request('gid');
    $group_name = request('gname');
    $terms = request('terms');

    $group = DB::table('kurssiryhma')
    ->where('kurssiryhma.ryhma_id', $gid)
    ->update(['kurssiryhma.ryhma_nimi' => $group_name]);

    $terms = DB::table('hakusanat')
    ->join('kurssit', 'hakusanat.kurssi_id','=','kurssit.ryhma_id')
    ->where('kurssit.ryhma_id','=',$gid)
    ->update(['hakusanat.hakusana' => $terms]);

    return redirect('/admintools');
}
```
`admin_group_add` funktiolle syötetään parametreina ryhmätunnus, ryhmän nimi ja sitä vastaavat hakutermit. Kun funktiota kutsutaan, se päivittää tiedot tietokantaan ja palauttaa takaisin oletusnäkymään.

## __4. Ajankäyttö__

Sanoisimme oman ajankäyttömme olevan hyvin samoilla viivoilla, sillä olimme aina yhdessä tekemässä projektia sovittuun aikaan. Kummatkin osallistuivat ideointiin ja tekemiseen omasta mielestämme erittäin tasavertaisesti. Työhön käytimme yhteen laskettuna noin 70 tuntia. Päätimme, että videoesittelyn tekeminen tulisi Aleksi Perälän harteille, koska hän omaa paljon selkeämmän puheäänen.
## __5. Itsearvio__

Mielestämme onnistuimme työssä suhteellisen hyvin, vaikka omat aikataulumme olivatkin hieman täydet. Puutteena voisimme huomioida mahdolliset pienet bugit ja olisimme voineet huomioida paremmin __tarkistukset__. Kaiken kaikkiaan ottaen huomioon että emme opiskele tietojenkäsittelyä ja nykyiset opintomme suuntautuvat Automaatioon, onnistuimme  mielestämme tässä tehtävässä hyvin.

Ehdotetut arvosanat: 
Mikael Nykänen (AB4510): __4__ 
Aleksi Perälä (P1725): __4__
