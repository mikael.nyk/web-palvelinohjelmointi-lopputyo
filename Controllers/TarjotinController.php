<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TarjotinController extends Controller
{
  public function list_all(){
    $courses = DB::table('kurssit')->paginate(6);

    return view('courses')->with('courses', $courses);
  }

  public function search(){
    $search = request('search');

    $searches = DB::table('kurssit')
    ->select('kurssit.kurssi_id',
    'kurssit.kurssi_nimi',
    'kurssit.kurssi_ects')
    ->distinct()
    ->join('hakusanat', 'kurssit.ryhma_id','=','hakusanat.kurssi_id')                       
    ->where('hakusanat.hakusana','LIKE','%'.$search.'%')
    ->orWhere('kurssit.kurssi_nimi','LIKE','%'.$search.'%')
    ->get();

    return view('searchcourses')->with('searches', $searches); 
  }

  public function search_admin(){
    $search = request('search');

    $searches = DB::table('kurssit')
    ->select('kurssit.kurssi_id',
    'kurssit.kurssi_nimi',
    'kurssit.kurssi_ects',
    'kurssit.ryhma_id')
    ->distinct()
    ->join('hakusanat', 'kurssit.ryhma_id','=','hakusanat.kurssi_id')                       
    ->where('hakusanat.hakusana','LIKE','%'.$search.'%')
    ->orWhere('kurssit.kurssi_nimi','LIKE','%'.$search.'%')
    ->get();

    return view('searchadmin')->with('searches', $searches); 
  }

  public function admin_list_all(){
    $courses = DB::table('kurssit')->paginate(6);

    return view('admintools')->with('courses', $courses);
  }

  public function admin_modify(){
    $id = request('id');

    $course = DB::table('kurssit')
    ->select('kurssit.kurssi_id',
    'kurssit.kurssi_nimi',
    'kurssit.kurssi_ects')                      
    ->where('kurssit.kurssi_id','=',$id)
    ->get();

    $group = DB::table('kurssiryhma')
    ->select('kurssiryhma.ryhma_id')
    ->distinct()
    ->join('kurssit', 'kurssiryhma.ryhma_id','=','kurssit.ryhma_id')
    ->where('kurssit.kurssi_id','=',$id)
    ->get();

    return view('courseform')
    ->with('course', $course)
    ->with('group', $group);
  }

  public function admin_update(){
    $id = request('id');
    $course_name = request('Name');
    $course_ects = request('ECTS');
    $gid = request('group');

    $course = DB::table('kurssit')
    ->where('kurssit.kurssi_id', $id)
    ->update(['kurssit.kurssi_nimi' => $course_name,
    'kurssit.kurssi_ects' => $course_ects,
    'kurssit.ryhma_id' => $gid]);

    return redirect('/admintools');
  }
  
  public function admin_delete(){
    $id = request('id');

    $course = DB::table('kurssit')
    ->where('kurssit.kurssi_id', $id)
    ->delete();

    return redirect('/admintools');
  }

  public function admin_add(){
    $course_name = request('Name_add');
    $course_ects = request('ECTS_add');
    $group_id = request('GID_add');

    $course = DB::table('kurssit')
    ->insert(['kurssit.ryhma_id' => $group_id,
    'kurssit.kurssi_nimi' => $course_name,
    'kurssit.kurssi_ects' => $course_ects]);

    return redirect('/admintools');
  }

  public function admin_add_check_course(){
    $min = DB::table('kurssiryhma')
    ->min('kurssiryhma.ryhma_id');

    $max = DB::table('kurssiryhma')
    ->max('kurssiryhma.ryhma_id');

    return view('/courseadd')
    ->with('min', $min)
    ->with('max', $max);
  }

  public function admin_group_modify(){
    $gid = request('gid');

    $group = DB::table('kurssiryhma')
    ->select('kurssiryhma.ryhma_id',
    'kurssiryhma.ryhma_nimi')                      
    ->where('kurssiryhma.ryhma_id','=',$gid)
    ->get();

    $terms = DB::table('hakusanat')
    ->select('hakusanat.hakusana')
    ->distinct()
    ->join('kurssit', 'hakusanat.kurssi_id','=','kurssit.ryhma_id')
    ->where('kurssit.ryhma_id','=',$gid)
    ->get();

    return view('groupform')
    ->with('group', $group)
    ->with('terms', $terms);
  }

  public function admin_group_update(){
    $gid = request('gid');
    $group_name = request('gname');
    $terms = request('terms');

    $group = DB::table('kurssiryhma')
    ->where('kurssiryhma.ryhma_id', $gid)
    ->update(['kurssiryhma.ryhma_nimi' => $group_name]);

    $terms = DB::table('hakusanat')
    ->join('kurssit', 'hakusanat.kurssi_id','=','kurssit.ryhma_id')
    ->where('kurssit.ryhma_id','=',$gid)
    ->update(['hakusanat.hakusana' => $terms]);

    return redirect('/admintools');
  }
  
  public function admin_group_delete(){
    $gid = request('gid');

    $group = DB::table('kurssiryhma')
    ->where('kurssiryhma.ryhma_id', $gid)
    ->delete();

    $terms = DB::table('hakusanat')
    ->where('hakusanat.kurssi_id', $gid)
    ->delete();
    
    return redirect('/admintools');
  }

  public function admin_group_add(){
    $gid = request('gid_add');
    $group_name = request('gname_add');
    $terms = request('terms_add');

    $group = DB::table('kurssiryhma')
    ->insert(['kurssiryhma.ryhma_id' => $gid,
    'kurssiryhma.ryhma_nimi' => $group_name]);

    $terms = DB::table('hakusanat')
    ->insert(['hakusanat.hakusana' => $terms,
    'hakusanat.kurssi_id' => $gid]);
    
    return redirect('/admintools');
  }

  public function admin_add_check_group(){
    $min = DB::table('kurssiryhma')
    ->min('kurssiryhma.ryhma_id');

    $max = DB::table('kurssiryhma')
    ->max('kurssiryhma.ryhma_id');

    return view('/groupadd')
    ->with('min', $min)
    ->with('max', $max);
  }
}