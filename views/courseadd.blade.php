@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add course</div>

                <div class="panel-body">

                <form method="POST" action="{{ url('/courseadd') }}">
                {{ csrf_field() }}
                    <div class="form-group row">
                    <label for="Course name" class="col-md-4 control-label">Course name:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="Name_add">
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="ECTS" class="col-md-4 control-label">ECTS:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" pattern="[1-9 ]+" title="1-999.." name="ECTS_add">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                    <label for="Group id" class="col-md-4 control-label">Group ID:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" pattern="[{{ $min }}-{{ $max }}]+" title="1-999... or the GID doesn't exist" name="GID_add">
                        </div>
                    </div><br>

                    <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Add course
                        </button>
                    </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection