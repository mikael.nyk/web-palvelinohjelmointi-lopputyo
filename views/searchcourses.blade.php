@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search results</div>

                <div class="panel-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Course name</th>
      <th scope="col">ECTS-Points</th>
    </tr>
  </thead>
  <tbody>

@foreach ($searches as $search)

    <tr class="bg-success">
      <th scope="row">{{ $search->kurssi_id }}</th>
      <td>{{ $search->kurssi_nimi }}</td>
      <td>{{ $search->kurssi_ects }}</td>
    </tr>

@endforeach

  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection