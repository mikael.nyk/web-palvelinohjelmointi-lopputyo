@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin tools</div>

                <div class="panel-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Course name</th>
      <th scope="col">ECTS-Points</th>
      <th scope="col">GID</th>
    </tr>
  </thead>
  <tbody>

@foreach ($courses as $course)

    <tr class="bg-success">
      <th scope="row">{{ $course->kurssi_id }}</th>
      <td><a href="{{ url('/courseform') }}?id={{ $course->kurssi_id }}">{{ $course->kurssi_nimi }}</a></td>
      <td>{{ $course->kurssi_ects }}</td>
      <td><a href="{{ url('/groupform') }}?gid={{ $course->ryhma_id }}">{{ $course->ryhma_id }}</a></td>
    </tr>

@endforeach

  </tbody>
</table>
{{ $courses->links() }}
                  <div class="form-group">
                  <div class="col-md-0.5 col-md-offset-0">
                    <a href="{{ url('/courseadd') }}">
                      <button type="submit" class="btn btn-primary">
                          Add course
                      </button>
                    </a>
                    <a href="{{ url('/groupadd') }}">
                      <button type="submit" class="btn btn-primary">
                          Add group
                      </button>
                    </a>
                  </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection