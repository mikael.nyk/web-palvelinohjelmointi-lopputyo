@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add group</div>

                <div class="panel-body">

                <form method="POST" action="{{ url('/groupadd') }}">
                {{ csrf_field() }}
                    <div class="form-group row">
                    <label for="Group id" class="col-md-4 control-label">Group ID:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" pattern="^(?!(?:0|[{{ $min }}-{{ $max }}])$)(?:[0-9]{1,2}|100)$" title="1-100... or the GID is already in use" name="gid_add">
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="Group name" class="col-md-4 control-label">Group name:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="gname_add">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                    <label for="Terms" class="col-md-4 control-label">Search terms:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="terms_add">
                        </div>
                    </div><br>
                    
                    <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Add group
                        </button>
                    </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection