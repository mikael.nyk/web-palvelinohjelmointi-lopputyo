@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modify or delete a group</div>

                <div class="panel-body">

@foreach ($group as $group_mod)
                <form method="POST" action="{{ url('/') }}/groupform/{{ $group_mod->ryhma_id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                    <div class="form-group row">
                    <label for="gid" class="col-md-4 control-label">GID:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="gid" value="{{ $group_mod->ryhma_id }}" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="gname" class="col-md-4 control-label">Group name:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="gname" value="{{ $group_mod->ryhma_nimi }}">
                        </div>
                    </div>

                    @foreach ($terms as $term)
                        <div class="form-group row">
                        <label for="Search terms" class="col-md-4 control-label">Search terms:</label>
                            <div class="col-md-6">
                            <input id="Name" type="text" name="terms" value="{{ $term->hakusana }}">
                            </div>
                        </div><br>                    
                    @endforeach

                    <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                    </div><br><br>
                </form>

                <form method="POST" action="{{ url('/') }}/groupform/{{ $group_mod->ryhma_id }}">
 
                @method('DELETE')
                @csrf
 
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Delete group
                        </button>
                    </div>
                    </div>
                </form>
@endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection