@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search results</div>

                <div class="panel-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Course name</th>
      <th scope="col">ECTS-Points</th>
      <th scope="col">GID</th>
    </tr>
  </thead>
  <tbody>

@foreach ($searches as $search)

    <tr class="bg-success">
      <th scope="row">{{ $search->kurssi_id }}</th>
      <td><a href="{{ url('/courseform') }}?id={{ $search->kurssi_id }}">{{ $search->kurssi_nimi }}</a></td>
      <td>{{ $search->kurssi_ects }}</td>
      <td><a href="{{ url('/groupform') }}?gid={{ $search->ryhma_id }}">{{ $search->ryhma_id }}</a></td>
    </tr>

@endforeach

  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection