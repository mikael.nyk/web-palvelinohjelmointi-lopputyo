@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">All Courses</div>

                <div class="panel-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Course name</th>
      <th scope="col">ECTS-Points</th>
    </tr>
  </thead>
  <tbody>

@foreach ($courses as $course)

    <tr class="bg-success">
      <th scope="row">{{ $course->kurssi_id }}</th>
      <td>{{ $course->kurssi_nimi }}</td>
      <td>{{ $course->kurssi_ects }}</td>
    </tr>

@endforeach

  </tbody>
</table>
{{ $courses->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection