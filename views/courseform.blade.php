@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modify or delete a course</div>

                <div class="panel-body">

@foreach ($course as $course_mod)
                <form method="POST" action="{{ url('/') }}/courseform/{{ $course_mod->kurssi_id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                    <div class="form-group row">
                    <label for="Course name" class="col-md-4 control-label">Course name:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="Name" value="{{ $course_mod->kurssi_nimi }}">
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="ECTS" class="col-md-4 control-label">ECTS:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" pattern="[1-9 ]+" title="1-999.." name="ECTS" value="{{ $course_mod->kurssi_ects }}">
                        </div>
                    </div>
                    @if (count($group) > 0)
                    @foreach ($group as $gid)                
                    <div class="form-group row">
                    <label for="Group" class="col-md-4 control-label">Course group:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="group" value="{{ $gid->ryhma_id }}">
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="form-group row">
                    <label for="Group" class="col-md-4 control-label">Course group:</label>
                        <div class="col-md-6">
                        <input id="Name" type="text" name="group">
                        </div>
                    </div>
                    @endif

                    <br>
                    <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                    </div><br><br>
                </form>

                <form method="POST" action="{{ url('/') }}/courseform/{{ $course_mod->kurssi_id }}">
 
                @method('DELETE')
                @csrf
 
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Delete course
                        </button>
                    </div>
                    </div>
                </form>
@endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection